package com.estudiantes.uber.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import com.estudiantes.uber.databinding.ActivityMainBinding
import com.estudiantes.uber.model.Cliente
import com.estudiantes.uber.services.Utility
import com.estudiantes.uber.viewmodel.MainViewModel
import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import com.google.gson.Gson

class MainActivity : AppCompatActivity() {

    private lateinit var  binding: ActivityMainBinding

    private val viewModel: MainViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.btnConductor.setOnClickListener {
            onclick();
        }
        val dataList =Cliente("asd","#","23","asdadas")

        var jsonString = formatJson(Gson().toJson(dataList))

        var testModel = Gson().fromJson(jsonString, Cliente::class.java)

        Utility.showToast(this,jsonString)

        Utility.showToast(this,testModel.nombre)

    }

    fun onclick(){
        val intent=Intent(this,login::class.java)
        Utility.isconductor=true
        startActivity(intent)
    }

    private fun formatJson(content: String): String {
        val gson = GsonBuilder().setPrettyPrinting().create()
        val jsonParser = JsonParser()
        val jsonElement = jsonParser.parse(content)
        return gson.toJson(jsonElement)
    }




}