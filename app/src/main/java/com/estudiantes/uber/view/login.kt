package com.estudiantes.uber.view

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.estudiantes.uber.databinding.ActivityLoginBinding
import com.estudiantes.uber.viewmodel.LoginViewModel

class login : AppCompatActivity() {

    private lateinit var biding: ActivityLoginBinding

    private val viewModel : LoginViewModel by viewModels();

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        biding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(biding.root)
        init()
    }


    /**
     *@Todo: Inicializar los componentes
     * */
    fun onclik() {
        biding.btncuenta.setOnClickListener {
            val intent = Intent(this, Registro::class.java)
            startActivity(intent)
            finish()
        }

        biding.btnLogin.setOnClickListener {
            val intent = Intent(this, verylogin::class.java)
            startActivity(intent)
            finish()
        }

    }

    fun init() {
        actionBar?.setDisplayHomeAsUpEnabled(true);
        toolbar()
        onclik()
    }

    @SuppressLint("UseSupportActionBar")
    fun toolbar() {
        biding.logintoobar.let {
            it?.title = "Login"
            setSupportActionBar(it)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowHomeEnabled(true)
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


}