package com.estudiantes.uber.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.estudiantes.uber.databinding.ActivityPantallainicioBinding

class Pantallainicio : AppCompatActivity() {

    private lateinit var binding: ActivityPantallainicioBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPantallainicioBinding.inflate(layoutInflater)
        setContentView(binding.root)

    }
}