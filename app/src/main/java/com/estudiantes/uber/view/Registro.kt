package com.estudiantes.uber.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.activity.viewModels
import com.estudiantes.uber.databinding.ActivityRegistroBinding
import com.estudiantes.uber.services.Utility
import com.estudiantes.uber.viewmodel.RegistroViewModel
import com.estudiantes.uber.services.Services.valido


class Registro : AppCompatActivity() {

    private lateinit var binding: ActivityRegistroBinding

    private  val viewModel : RegistroViewModel  by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegistroBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()

    }

    fun init(){
        events()
        toolbar()
        viewModel.valido.observe( this){
            if (valido==it){
                Utility.showToast(this,"Registro exitoso")

                finish()
            }else{
                Utility.showToast(this,it)
            }

        }
    }

    fun events(){
        binding.btnRegistrar.setOnClickListener {
          viewModel.createUser(binding.correo.text,binding.contraseA.text,binding.nombre.text,binding.telefono.text,
              this,layoutInflater)
        }
    }

    fun toolbar(){
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.title = "Registro"
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }



}