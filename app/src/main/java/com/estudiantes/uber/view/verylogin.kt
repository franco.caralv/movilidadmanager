package com.estudiantes.uber.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.activity.viewModels
import com.estudiantes.uber.databinding.ActivityVeryloginBinding
import com.estudiantes.uber.services.Services
import com.estudiantes.uber.services.Utility
import com.estudiantes.uber.viewmodel.veryloginViewModel

class verylogin : AppCompatActivity() {

    private lateinit var binding: ActivityVeryloginBinding

    private  val viewModel : veryloginViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityVeryloginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }


    fun init(){
        events()
        toolbar()
        viewModel.result.observe(
            this,
        ) { result ->
            if (result.isNotEmpty()) {
                if (result == Services.valido) {
                    Utility.showToast(this, "Bienvenido")
                    /*finish()*/
                } else {
                    binding.nombre.setText("")
                    binding.contrasena.setText("")
                    Utility.showToast(this, result);
                }
            }
        }
    }

    fun events(){

        binding.iniciosesion.setOnClickListener {
            viewModel.vereficadousver(binding.nombre.text,  binding.contrasena.text,this,layoutInflater)
        }

    }

    fun toolbar(){
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.title = "Login"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}