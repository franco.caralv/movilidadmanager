package com.estudiantes.uber.model

data class Conductor(
    val nombre: String ?= "",
    val apellido: String? = "",
    val correo: String  = "",
    val foto: String    = "",
    val telefono: String = "",
    val vehiculo: String = "",
    val placa: String = "",
    val estado: Boolean = false)
