package com.estudiantes.uber.model

data class Cliente(
    val nombre: String,
    val apellido: String,
    val correo: String,
    val telefono: String,
    val direccion: String= "",
    val latitud: Double = 0.0,
    val longitud: Double= 0.0
)
