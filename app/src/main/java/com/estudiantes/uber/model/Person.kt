package com.estudiantes.uber.model

data class Person(
    val name: String,
    val password: String
)
