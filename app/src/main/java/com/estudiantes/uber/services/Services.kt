package com.estudiantes.uber.services

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import com.estudiantes.uber.R
import com.estudiantes.uber.databinding.ActivityPantallainicioBinding
import com.estudiantes.uber.databinding.DialogolayoutBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase

object Services {


    const val BASE_URL = "https://api.uber.com/v1.2/"

    const val GET_PRODUCTS = "products"

    const val valido = "Usuario creado"

    val auth =FirebaseAuth.getInstance()

    val database=FirebaseDatabase.getInstance().reference

    const val CAMPOS_VACIOS = "\"Todos los campos son obligatorios\""

    fun getcratedialogo(context: Context, layoutInflater: LayoutInflater): AlertDialog? {
        val dialogo=AlertDialog.Builder(context)
        val biding= DialogolayoutBinding.inflate(layoutInflater)
        dialogo.setView(biding.root)
        return  dialogo.create()
    }

    fun setsharestpreferes(context: Context ,keys:String){
        val sharedPreferences=context.getSharedPreferences("com.estudiantes.uber",Context.MODE_PRIVATE)
        val editor=sharedPreferences.edit()
        editor.putString("nombre","")
        editor.putString("apellido","")
        editor.putString("telefono","")
        editor.putString("correo","")
        editor.putString("contrasena","")
        editor.apply()
    }

    const val ERROR_CONEXION = "\"Error de conexión\""

    const val NOMBRE = "\"El nombre no puede estar vacío\""

    const val NO_VALIDO = "\"No es válido\""

    const val CORREO = "\"El correo no puede estar vacío\""

    const val CONTRASENNA = "\"La contraseña no puede estar vacía\""

    const val CONTRASENIA_CORTA = "\"La contraseña debe tener al menos 6 caracteres\""

    const val CORRER_ARROBA = "\"El correo debe tener un arroba\""

    const val ERROR_EMAIL = "\"El email no es valido\""

    const val ERROR_PASSWORD_CONFIRM = "\"Las contraseñas no coinciden\""

    const val ERROR_USER_EXIST = "\"El usuario ya existe\""

    const val ERROR_USER_NOT_EXIST = "\"El usuario no existe\""

    const val ERROR_USER_NOT_LOGIN = "\"El usuario no esta logueado\""

    const val ERROR_USER_NOT_LOGOUT = "\"El usuario no se pudo desloguear\""

    const val ERROR_USER_NOT_UPDATE = "\"El usuario no se pudo actualizar\""

    const val ERROR_USER_NOT_DELETE = "\"El usuario no se pudo eliminar\""

    const val ERROR_USER_NOT_CREATE = "\"El usuario no se pudo crear\""

    const val ERROR_USER_NOT_FIND = "\"El usuario no se pudo encontrar\""

    const val ERROR_USER_NOT_LOGIN_FACEBOOK = "\"El usuario no se pudo loguear con facebook\""

    const val ERROR_USER_NOT_LOGIN_GOOGLE = "\"El usuario no se pudo loguear con google\""

    const val ERROR_USER_NOT_LOGIN_TWITTER = "\"El usuario no se pudo loguear con twitter\""

}