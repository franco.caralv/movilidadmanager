package com.estudiantes.uber.services

import android.content.Context
import android.widget.Toast

class Utility {

    companion object {
        fun getDistance(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
            val theta = lon1 - lon2
            var dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta))
            dist = Math.acos(dist)
            dist = rad2deg(dist)
            dist = dist * 60 * 1.1515
            return dist
        }

        fun deg2rad(deg: Double): Double {
            return deg * Math.PI / 180.0
        }

        fun rad2deg(rad: Double): Double {
            return rad * 180.0 / Math.PI
        }

        var isconductor = false

        fun showToast(context: Context, message: String) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }

    }

  }