package com.estudiantes.uber.viewmodel

import android.content.Context
import android.text.Editable
import android.view.LayoutInflater
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.estudiantes.uber.model.Cliente
import com.estudiantes.uber.model.Conductor
import com.estudiantes.uber.services.Services.CAMPOS_VACIOS
import com.estudiantes.uber.services.Services.CONTRASENIA_CORTA
import com.estudiantes.uber.services.Services.CORRER_ARROBA
import com.estudiantes.uber.services.Services.ERROR_EMAIL
import com.estudiantes.uber.services.Services.valido as validoServices
import com.estudiantes.uber.services.Services.auth
import com.estudiantes.uber.services.Services.database
import com.estudiantes.uber.services.Services.getcratedialogo
import com.estudiantes.uber.services.Utility

class RegistroViewModel : ViewModel() {

    var life: MutableLiveData<Boolean> = MutableLiveData()

    var Conductor: MutableLiveData<Conductor> = MutableLiveData()

    var Cliente: MutableLiveData<Cliente> = MutableLiveData()

    var valido: MutableLiveData<String> = MutableLiveData()

    var Conductorrjx: Conductor? = null

    var Clienterjx: Cliente? = null

    fun createUser(
        email: Editable?,
        password: Editable?,
        nombre: Editable?,
        telefono: Editable?,
        context: Context,
        layoutInflater: LayoutInflater
    ) {
        val dialo= getcratedialogo(context,layoutInflater)
        val emailtx = email.toString()
        val passwordtx = password.toString()
        val nombretx = nombre.toString()
        val telefonotx = telefono.toString()
        dialo!!.show()
        life.value = true
        if (emailtx.isNullOrEmpty() || passwordtx.isNullOrEmpty() || nombretx.isNullOrEmpty() || telefonotx.isNullOrBlank()) {
            valido.value = CAMPOS_VACIOS
            dialo.hide()
            return
        }
        if (!emailtx.contains("@")) {
            valido.value = CORRER_ARROBA
            dialo.hide()
            return
        }
        if (passwordtx.length < 6) {
            valido.value = CONTRASENIA_CORTA
            dialo.hide()
            return
        }
        if (Utility.isconductor) {
            Conductorrjx = Conductor(emailtx, passwordtx, nombretx, telefonotx)
            Conductor.value = Conductorrjx
            auth.createUserWithEmailAndPassword(emailtx, passwordtx).addOnCompleteListener {
                if (it.isSuccessful) {
                    saveUser()
                } else {
                    valido.value = ERROR_EMAIL

                }
                dialo.hide()
            }
        } else {
            Clienterjx = Cliente(emailtx, passwordtx, nombretx, telefonotx)
            Cliente.value = Clienterjx
            auth.createUserWithEmailAndPassword(emailtx, passwordtx).addOnCompleteListener {
                if (it.isSuccessful) {
                    saveUser()
                } else {
                    valido.value = ERROR_EMAIL
                }
                dialo.hide()
            }
        }
    }

    fun saveUser() {
        if (Utility.isconductor) {
            database.child("Users").child("Conductores").setValue(Conductorrjx!!).addOnCompleteListener {
                valido.value = validoServices
            }
        }
        else {
            database.child("Users").child("Clientes").setValue(Clienterjx!!).addOnCompleteListener{
                valido.value =validoServices
            }
        }

    }


}