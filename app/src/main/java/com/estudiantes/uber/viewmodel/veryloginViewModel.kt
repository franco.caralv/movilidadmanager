package com.estudiantes.uber.viewmodel

import android.content.Context
import android.text.Editable
import android.view.LayoutInflater
import android.widget.EditText
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.estudiantes.uber.model.Person
import com.estudiantes.uber.services.Services


class veryloginViewModel : ViewModel() {

    var persona: MutableLiveData<Person> = MutableLiveData()

    var result: MutableLiveData<String> = MutableLiveData()


    fun setPerson(person: Person) {
        persona.value = person
    }

    fun vereficadousver(
        email: Editable?,
        password: Editable?,
        context: Context,
        layoutInflater: LayoutInflater) {
        val dialo= Services.getcratedialogo(context,layoutInflater)
        dialo!!.show()
        email.let { emailtx ->
            password.let { passwordtx ->
                val texemail = emailtx.toString()
                val texpassword = passwordtx.toString()
                if((emailtx.isNullOrEmpty() || passwordtx.isNullOrEmpty())){
                    dialo.hide()
                    result.value = Services.CAMPOS_VACIOS
                    return
                }
                if (texemail == "" && texpassword == "") {
                    result.value= Services.CAMPOS_VACIOS
                }
                if (texemail == "") {
                    result.value= Services.NOMBRE
                }
                if (texpassword == "") {
                    result.value= Services.CONTRASENNA
                }
                if (texpassword.length < 6) {
                    result.value= Services.CONTRASENIA_CORTA
                }
                Services.auth.signInWithEmailAndPassword(texemail, texpassword).addOnCompleteListener {
                    if (it.isSuccessful) {
                        result.value= Services.valido
                    } else {
                        result.value= Services.NO_VALIDO
                    }
                    dialo.hide()
                }
            }?: run {
                dialo.hide()
                this@veryloginViewModel.result.value= Services.NO_VALIDO
            }
        }?: run {
            dialo.hide()
            this@veryloginViewModel.result.value= Services.NO_VALIDO
        }



    }




}